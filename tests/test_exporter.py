import pytest 
import decimal
from google_workspace_utils.exporter import GoogleSheetsExporter

@pytest.fixture
def decimal_val():
    return decimal.Decimal(0.333)
@pytest.fixture

def formula_val():
    return "=B2+E2"

@pytest.fixture
def rows(decimal_val, formula_val):
    return [
        ['bool', 'int', 'str', 'decimal',   'formula',   'currency $', 'net_value_usd', 'price', 'percent %', 'profit ratio', ''],
        [True,   1,     "foo", decimal_val, formula_val, 1000.44,      333.66,          4.56,    0.6667,      0.123,          None]
    ]

@pytest.fixture(scope='module')
def sheets_export():
    return GoogleSheetsExporter()

@pytest.fixture
def sheet_data_params(sheets_export, rows):
    return sheets_export._GoogleSheetsExporter__update_rows_in_sheet('dummy_sheet_id', rows)

@pytest.fixture
def sheet_data(sheet_data_params):
    return sheet_data_params['updateCells']['rows']

def test_bool_value(sheet_data):
    properties = sheet_data[1]['values'][0]
    assert 'boolValue' in properties['userEnteredValue']
    assert properties['userEnteredValue']['boolValue'] is True

def test_int_value(sheet_data):
    properties = sheet_data[1]['values'][1]
    assert 'numberValue' in properties['userEnteredValue']
    assert properties['userEnteredValue']['numberValue'] is 1
    assert 'numberFormat' in properties['userEnteredFormat']
    assert 'type' in properties['userEnteredFormat']['numberFormat']
    assert properties['userEnteredFormat']['numberFormat']['type'] is 'NUMBER'

def test_str_value(sheet_data):
    properties = sheet_data[1]['values'][2]
    assert 'stringValue' in properties['userEnteredValue']
    assert properties['userEnteredValue']['stringValue'] is "foo"

def test_decimal_value(sheet_data, decimal_val):
    properties = sheet_data[1]['values'][3]
    assert 'numberValue' in properties['userEnteredValue']
    assert properties['userEnteredValue']['numberValue'] == str(decimal_val)
    assert 'numberFormat' in properties['userEnteredFormat']
    assert 'type' in properties['userEnteredFormat']['numberFormat']
    assert properties['userEnteredFormat']['numberFormat']['type'] is 'NUMBER'

def test_formula_value(sheet_data, formula_val):
    properties = sheet_data[1]['values'][4]
    assert 'formulaValue' in properties['userEnteredValue']
    assert properties['userEnteredValue']['formulaValue'] is formula_val

def test_currency_value(sheet_data):
    properties = sheet_data[1]['values'][5]
    assert 'numberValue' in properties['userEnteredValue']
    assert properties['userEnteredValue']['numberValue'] is 1000.44
    assert 'numberFormat' in properties['userEnteredFormat']
    assert 'type' in properties['userEnteredFormat']['numberFormat']
    assert properties['userEnteredFormat']['numberFormat']['type'] is 'CURRENCY'

def test_net_value_usd_value(sheet_data):
    properties = sheet_data[1]['values'][6]
    assert 'numberValue' in properties['userEnteredValue']
    assert properties['userEnteredValue']['numberValue'] is 333.66
    assert 'numberFormat' in properties['userEnteredFormat']
    assert 'type' in properties['userEnteredFormat']['numberFormat']
    assert properties['userEnteredFormat']['numberFormat']['type'] is 'CURRENCY'

def test_price_value(sheet_data):
    properties = sheet_data[1]['values'][7]
    assert 'numberValue' in properties['userEnteredValue']
    assert properties['userEnteredValue']['numberValue'] is 4.56
    assert 'numberFormat' in properties['userEnteredFormat']
    assert 'type' in properties['userEnteredFormat']['numberFormat']
    assert properties['userEnteredFormat']['numberFormat']['type'] is 'CURRENCY'

def test_percent_value(sheet_data):
    properties = sheet_data[1]['values'][8]
    assert 'numberValue' in properties['userEnteredValue']
    assert properties['userEnteredValue']['numberValue'] is 0.6667
    assert 'numberFormat' in properties['userEnteredFormat']
    assert 'type' in properties['userEnteredFormat']['numberFormat']
    assert properties['userEnteredFormat']['numberFormat']['type'] is 'PERCENT'

def test_ratio_value(sheet_data):
    properties = sheet_data[1]['values'][9]
    assert 'numberValue' in properties['userEnteredValue']
    assert properties['userEnteredValue']['numberValue'] is 0.123
    assert 'numberFormat' in properties['userEnteredFormat']
    assert 'type' in properties['userEnteredFormat']['numberFormat']
    assert properties['userEnteredFormat']['numberFormat']['type'] is 'PERCENT'

def test_blank_value(sheet_data):
    properties = sheet_data[1]['values'][10]
    assert 'numberValue' in properties['userEnteredValue']
    assert properties['userEnteredValue']['numberValue'] is None
