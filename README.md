# Google Workspace Utilities

Utilities library for interacting with Google Workspace APIs in Python, namely Google Sheets and Google Drive.

## Local development
* Go to GCP Cloud console
* Go to the APIs and Services area
* Credentials -> Create Credentials -> OAuth Client ID
* Application Type: Desktop app
* Give a clear name like "hedge manager local development" 
* Save
* Download JSON file and name as `credentials.json` in your app's root directory
* Make sure that file is in your `.gitignore`!
* when you run for the first time you'll be prompted to sign in, to enable the necessary APIs, etc.
* double check the `SCOPES` your app is asking for if you're having issues.

## Deployment
* Use a service account - create the account then grant View or Edit access to the service acct email address on the documents, folders, etc. that it needs access to
* When running your compute instance or cloud run service, make sure the service account is used to run it, so that its access to Drive/Sheets/etc is inherited.

# Examples

##  Include in a project's `requirements.txt`:

```requirements.txt
google_workspace_utils @ git+https://gitlab.com/firology/google-workspace-utils.git # after pushing commits, change this comment to force docker to re-run pip install.
```

## Export to Sheets

# Simple export to new sheet, append, then append or create a new sheet

```python
from google_workspace_utils import GoogleSheetsExporter, timestamp
# The ID from the Google Sheets URL
sheet_id = "1gABucQI4JJF4zNXtO-BuB2FEI9ooEkvNuc9YdIbJMcw"
# Connect to the service
sheets = GoogleSheetsExporter(sheet_id)
# Sample data
rows = [["column 1", "column 2", "some other column", "asdf"], [1, 2, "fish", True], ["monkey", 6, False, 8]]
# Sheet titles must be unique so insert the date and time to avoid name clashes
sheet_title = f"{timestamp()}-demo-data"
# Create a new sheet
sheets.create_new_sheet(rows, sheet_title)

new_rows = [[1,2,3,4],[1,2,3,4],[1,2,3,4]]
# Add to existing sheet - This will only work if sheet title already exists
sheets.append_to_sheet(new_rows, sheet_title)
# Create a new sheet, or it already exists, add to it (ignores first row so as not to duplicate column headers)
sheets.append_or_create_sheet(new_rows, "demo_data")
sheets.append_or_create_sheet(new_rows, "demo_data")
```
