"""
Export tabular data to Google Sheets.
"""

import datetime
import decimal
import logging
import re
from math import isnan as math_isnan
from sqlite3 import Timestamp

import pandas as pd
from googleapiclient.errors import HttpError
from numpy import isnan as np_isnan

from google_workspace_utils.connector import (GoogleConnector, execute_request,
                                              sheet_url)
from google_workspace_utils.date_formats import timestamp

PERCENTAGE_PATTERN = ".*(/|%|ratio|percent|rate).*"
CURRENCY_PATTERN = ".*(\$|value|usd|price|profit).*"


class GoogleSheetsExporter():

    def __init__(self, spreadsheet_id=None):
        self.service = None
        self.spreadsheet_id = None
        self.pending_creates = []
        self.pending_fills = []
        self.sheet_title_ids = {}
        assert spreadsheet_id, "spreadsheet_id must be provided."
        self.spreadsheet_id = spreadsheet_id
        self.already_imported_ids = False
        self.init_google_sheets()

    def init_google_sheets(self):
        connector = GoogleConnector('sheets', 'v4')
        self.service = connector.service() 
        self.sheets = self.service.spreadsheets()
        self.__set_sheet_ids_by_import()

    def create_new_sheet(self, rows, sheet_title=None):
        num_rows = len(rows)
        num_columns = len(rows[0])
        sheet_title = self.__avoid_sheet_title_conflict(sheet_title)
        params = {'requests': [self.__new_sheet(num_rows, num_columns, sheet_title)]}
        response = self.__exec_batch_update(params)
        sheet_id = response.get('replies')[0].get('addSheet').get('properties').get('sheetId')

        params = {'requests': [
            self.__update_rows_in_sheet(sheet_id, rows),
            self.__auto_column_width(sheet_id, num_columns)
        ]}
        response = self.__exec_batch_update(params)
        self.sheet_title_ids[sheet_title] = sheet_url(self.spreadsheet_id, sheet_id)
        return self.sheet_title_ids[sheet_title]

    def create_new_sheet_later(self, rows, sheet_title=None):
        num_rows = len(rows)
        num_columns = len(rows[0])
        sheet_title = self.__avoid_sheet_title_conflict(sheet_title)
        params = self.__new_sheet(num_rows, num_columns, sheet_title)
        self.pending_creates.append(params)
        self.pending_fills.append({'sheet_title': sheet_title, 'rows': rows, 'num_columns': num_columns})

    def execute_pending_creates(self):
        if len(self.pending_creates) == 0:
            logging.error("No pending creates to execute.")
            return
        params = {'requests': self.pending_creates}
        response = self.__exec_batch_update(params)
        self.__set_sheet_ids_from_add_response(response)
        self.pending_creates = []
        fill_requests = []
        for pending_fill in self.pending_fills:
            sheet_id = self.sheet_title_ids.get(pending_fill['sheet_title'])
            if not sheet_id:
                logging.error(f"Could not find sheet id for {pending_fill['sheet_title']}")
                continue
            fill_requests += [
                self.__update_rows_in_sheet(sheet_id, pending_fill['rows']),
                self.__auto_column_width(sheet_id, pending_fill['num_columns'])
            ]
        params = {'requests': fill_requests}
        response = self.__exec_batch_update(params)
        self.pending_fills = []

    def append_to_sheet(self, rows, sheet_title):
        params = {
            "majorDimension": 'ROWS',
            "values": rows # self.__value_range_row_data(rows)
        }
        request = self.sheets.values().append(
            spreadsheetId=self.spreadsheet_id, 
            range=f'{sheet_title}!A1:B2',
            valueInputOption='USER_ENTERED',
            insertDataOption='INSERT_ROWS',
            body=params
        )
        response = execute_request(request)

    def append_or_create_sheet(self, rows, sheet_title):
        self.__set_sheet_ids_by_import()
        if sheet_title in self.sheet_title_ids:
            self.append_to_sheet(rows[1:], sheet_title)
        else:
            self.create_new_sheet(rows, sheet_title)
        return self.sheet_title_ids[sheet_title]

    def get_sheet_url(self, sheet_title):
        sheet_id = self.sheet_title_ids.get(sheet_title)
        if sheet_id is None:
            msg = f"sheet titled {sheet_title} not found."
            logging.error(msg)
            return msg
        return sheet_url(self.spreadsheet_id, sheet_id)

    def __exec_batch_update(self, params):
        request = self.sheets.batchUpdate(spreadsheetId=self.spreadsheet_id, body=params)
        try:
            response = execute_request(request)
        except HttpError as e:
            logging.error(f"batch update failed: {e}")
            multi_create_400_err_pattern = r'addSheet: A sheet with the name ".*" already exists'
            multi_create_400_err_match = re.search(multi_create_400_err_pattern, e.reason)
            if e.status_code == 400 and multi_create_400_err_match:
                logging.error(f"Could not create sheet because it already exists. Continuing.")
                return None
            raise e
        return response

    def __set_sheet_ids_from_add_response(self, response):
        if not response:
            logging.error("No API response from batch update, cannot set sheet ids.")
            self.__set_sheet_ids_by_import()
            return
        replies = response.get('replies')
        for reply in replies:
            sheet_props = reply.get('addSheet').get('properties')
            if not sheet_props:
                logging.error("Couldn't find addSheet properties in API add response.")
            sheet_id = sheet_props.get('sheetId')
            sheet_title = sheet_props.get('title')
            if not sheet_title or not sheet_id:
                logging.error(f"Missing data in API response addSheet properties. sheet title: {sheet_title} sheet id: {sheet_id}")
            self.sheet_title_ids[sheet_title] = sheet_id

    def __set_sheet_ids_by_import(self):
        if self.already_imported_ids:
            logging.warning(f"Already imported sheet ids, avoiding another request.")
            return
        request = self.sheets.get(spreadsheetId=self.spreadsheet_id)
        response = execute_request(request)
        sheets = response.get('sheets')
        for sheet in sheets:
            sheet_title = sheet.get('properties').get('title')
            sheet_id = sheet.get('properties').get('sheetId')
            self.sheet_title_ids[sheet_title] = sheet_id
        self.already_imported_ids = True
        
    def __avoid_sheet_title_conflict(self, sheet_title):
        if sheet_title in self.sheet_title_ids:
            sheet_title = f"+{sheet_title}"
        return sheet_title

    def __new_sheet(self, num_rows, num_columns, sheet_title=None):
        if not sheet_title:
            sheet_title = timestamp()
        frozen_rows = 1
        if not num_rows or num_rows < 2:
            logging.warn(f"exporting to new sheet '{sheet_title}' with no rows.")
            frozen_rows = 0
        params = {
            'addSheet': {
                'properties': {
                    'title': sheet_title,
                    'index': 0,  # always move to the front
                    'gridProperties': {
                        'rowCount': num_rows,
                        'columnCount': num_columns,
                        'frozenRowCount': frozen_rows
                    }
                }
            }
        }
        return params

    def __entered_value(self, cell):
        entered_value = {}
        if isinstance(cell, str):
            entered_value = {'stringValue': cell}
            if len(cell) > 0 and cell[0] == '=':
                entered_value = {'formulaValue': cell}
        elif isinstance(cell, bool):
            entered_value = {'boolValue': cell}
        elif isinstance(cell, decimal.Decimal):
            entered_value = {'numberValue': str(cell)}
        elif isinstance(cell, datetime.timedelta) or isinstance(cell, pd.Timestamp):
            entered_value = {'stringValue': str(cell)}
        elif cell is None or math_isnan(cell) or np_isnan(cell):
            entered_value = {'numberValue': None}
        else:
            entered_value = {'numberValue': cell}
        return entered_value

    def __entered_format(self, value, column_name):
        entered_format = {'horizontalAlignment': "CENTER"}
        if 'numberValue' in value:
            entered_format['numberFormat'] = {'type': 'NUMBER'}
            if isinstance(column_name, str):
                percentage_match = re.match(PERCENTAGE_PATTERN, column_name, re.IGNORECASE)
                if percentage_match:
                    entered_format['numberFormat'] = {'type': 'PERCENT'}
                else:
                    currency_match = re.match(CURRENCY_PATTERN, column_name, re.IGNORECASE)
                    if currency_match:
                        entered_format['numberFormat'] = {'type': 'CURRENCY'}
        return entered_format

    def __value_range_row_data(self, rows):
        all_row_data = []
        columns = rows[0] if rows else None
        for row in rows:
            row_cells = []
            for i,cell in enumerate(row):
                entered_value = self.__entered_value(cell)
                entered_format = self.__entered_format(entered_value, columns[i])
                row_cells.append({
                    'userEnteredValue': entered_value,
                    'userEnteredFormat': entered_format
                })
            all_row_data.append({'values': row_cells})
        return all_row_data

    def __update_rows_in_sheet(self, sheet_id, rows):
        params = {
            'updateCells': {
                'start': {
                    'sheetId': sheet_id,
                    'rowIndex': 0,
                    'columnIndex': 0
                },
                'fields': '*',
                'rows': self.__value_range_row_data(rows)
            }
        }
        return params

    def __auto_column_width(self, sheet_id, num_columns):
        params = {
            "autoResizeDimensions": {
                "dimensions": {
                    "sheetId": sheet_id,
                    "dimension": "COLUMNS",
                    "startIndex": 0,
                    "endIndex": num_columns
                }
            }
        }
        return params


if __name__ == '__main__':
    sheets = GoogleSheetsExporter()
    sheets.append_to_sheet([[1, 2, "fish", True], ["monkey", 6, False, 8]])
