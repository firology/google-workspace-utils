import logging
from mimetypes import guess_type

from googleapiclient.errors import HttpError
from googleapiclient.http import MediaFileUpload

from google_workspace_utils.connector import GoogleConnector, execute_request

SUPPORTED_EXPORT_TYPES = {
    'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'csv': 'text/csv',
}


class GoogleDriveExporter:

    def __init__(self, scopes=None):
        self.scopes = scopes
        self.service = None
        self.init_google_drive()

    def init_google_drive(self):
        kwargs = {}
        if self.scopes:
            kwargs['scopes'] = self.scopes
        connector = GoogleConnector('drive', 'v3', **kwargs)
        self.service = connector.service()
        self.files = self.service.files()
    
    def find_or_create_folder(self, name, parents=[]):
        assert len(parents) > 0, "You must specify at least one parent folder"
        folder_id = self.find_folder_by_name_and_parent(name, parents[0])
        if folder_id is None:
            folder_id = self.create_folder(name, parents)
        return folder_id

    def find_folder_by_name_and_parent(self, name, parent):
        query = f"mimeType = 'application/vnd.google-apps.folder' " \
                f"and name = '{name}' " \
                f"and '{parent}' in parents"
        request = self.files.list(q=query, spaces="drive", fields='files(id, name)')
        response = execute_request(request)
        folders = response.get('files', [])
        if len(folders) > 0:
            folder = folders[0]
            folder_id = folder.get('id')
            folder_name = folder.get('name')
            logging.info(f"Found existing folder named '{folder_name}' and id {folder_id} when searching for folder '{name}' with parent {parent}.")
            return folder_id
        return None

    def create_folder(self, name, parents=[]):
        assert len(parents) > 0, "You must specify at least one parent folder"
        file_metadata = {
            # TODO: Figure out how to support Shared Drives
            # 'supportsAllDrives': 'true',
            # 'includeItemsFromAllDrives': 'true',
            'name': name,
            'mimeType': 'application/vnd.google-apps.folder',
            'parents': parents
        }
        request = self.files.create(body=file_metadata, fields='id')
        response = execute_request(request)
        return response.get('id')

    def create_file(self, file_path, parents):
        filename = file_path.split('/')[-1]
        file_metadata = {
            # TODO: Figure out how to support Shared Drives
            # 'supportsAllDrives': 'true',
            # 'includeItemsFromAllDrives': 'true',
            'name': filename,
            'parents': parents
        }
        mime_type = guess_type(file_path)[0]
        media = MediaFileUpload(file_path, mimetype=mime_type, resumable=True)
        request = self.files.create(body=file_metadata, media_body=media, fields='id')
        response = execute_request(request)
        return response.get('id')

    def list_files(self, query):
        request = self.files.list(q=query, pageSize=1000)
        response = execute_request(request)
        return response.get('files', [])

    def get_file_name(self, file_id):
        request = self.files.get(fileId=file_id, fields='name')
        response = execute_request(request)
        return response.get('name')

    def upload_files_to_folder(self, folder_name, files_list, parents=[]):
        assert len(parents) > 0, "You must specify at least one parent folder"
        folder_id = self.create_folder(folder_name, parents)
        for file_path in files_list:
            file_id = self.create_file(file_path, [folder_id])
        return folder_id

    def get_folder_url(self, folder_id):
        return f"https://drive.google.com/drive/folders/{folder_id}"

    def download_file(self, file_id, file_path, file_type='xlsx'):
        import io

        from googleapiclient.http import MediaIoBaseDownload

        mime_type = SUPPORTED_EXPORT_TYPES[file_type]
        try:
            request = self.files.export_media(fileId=file_id, mimeType=mime_type)
            file = io.FileIO(file_path, 'wb')
            downloader = MediaIoBaseDownload(file, request)
            done = False
            while done is False:
                status, done = downloader.next_chunk()
                logging.info(f"Download {int(status.progress() * 100)}%.")
        except HttpError as error:
            logging.error(F'An error occurred: {error}')
            file = None
            raise error
        finally:
            file.close()
        return file_path


def main():
    gdrive = GoogleDriveExporter()
    folder_name = 'test_folder'
    filenames = ['test.txt']
    folder_id = gdrive.upload_files_to_folder(folder_name, filenames)
    folder_url = gdrive.get_folder_url(folder_id)
    print(f"Folder uploaded: {folder_url}")


if __name__ == "__main__":
    main()
