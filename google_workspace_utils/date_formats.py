import datetime

def now():
    return datetime.datetime.utcnow()

def today():
    return now().date()

def yesterday():
    return days_ago(1)

def days_ago(days_ago):
    return today() - datetime.timedelta(days=days_ago)

def sheet_title_date(date):
    return format_date(date, "%Y%m%d")

def sheet_column_date(date):
    return format_date(date, "%Y-%m-%d")

def format_date(date, format):
    return datetime.datetime.strftime(date, format)

def timestamp():
    return format_date(now(), '%Y%m%d-%H%M%S')
