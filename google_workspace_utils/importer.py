"""Import data from google sheets"""

from google_workspace_utils.connector import GoogleConnector, sheet_url, execute_request
import re
import datetime
import pandas as pd


class GoogleSheetsImporter:

    def __init__(self, spreadsheet_id=None):
        self.service = None
        self.spreadsheet_id = None
        self.sheet_title_ids = {}
        assert spreadsheet_id is not None, "Spreadsheet ID must be provided"
        self.init_google_sheets()
        self.spreadsheet_id = spreadsheet_id

    def init_google_sheets(self):
        connector = GoogleConnector('sheets', 'v4')
        self.service = connector.service()
        self.sheets = self.service.spreadsheets()

    def get_all_sheet_titles(self):
        request = self.sheets.get(spreadsheetId=self.spreadsheet_id)
        response = execute_request(request)
        self.sheet_title_ids |= {
            sheet['properties']['title']: sheet['properties']['sheetId']
            for sheet in response['sheets']
        }
        return [sheet['properties']['title'] for sheet in response['sheets']]

    def get_sheet_id(self, sheet_title):
        if sheet_title in self.sheet_title_ids:
            return self.sheet_title_ids[sheet_title]
        request = self.sheets.get(spreadsheetId=self.spreadsheet_id)
        response = execute_request(request)
        find_title = [
            sheet['properties']['sheetId'] for sheet in response['sheets'] if sheet['properties']['title'] == sheet_title
        ]
        if len(find_title) == 0:
            return "worksheet_not_found"
        sheet_id = find_title[0]
        self.sheet_title_ids[sheet_title] = sheet_id
        return sheet_id

    def get_sheet_url(self, sheet_title):
        sheet_id = self.get_sheet_id(sheet_title)
        return sheet_url(self.spreadsheet_id, sheet_id)

    def get_sheet_data(self, sheet_title):
        request = (
            self.sheets.values().get(
                spreadsheetId=self.spreadsheet_id,
                range=sheet_title,
                valueRenderOption="UNFORMATTED_VALUE",
                majorDimension="ROWS"
            )
        )
        response = execute_request(request)
        assert 'values' in response and response['values'], f"No values found in Sheets response: {response}"
        return response['values']

    # TODO: move this to the Coindex report it is used for and out of this package
    def latest_sheet_info(self, config):
        sheet_infos = []
        sheet_titles = self.get_all_sheet_titles()
        datetime_search = re.compile(config['datetime_regex_pattern'])
        sheets_to_retrieve = config['sheets_to_retrieve']
        sheets_retrieved = 0
        datetime_pattern = config['datetime_strptime_pattern']
        for sheet_title in sheet_titles:
            sheet_info = {}
            datetime_match = datetime_search.match(sheet_title)
            if not datetime_match:
                continue
            datetime_str = datetime_match.group(config['date_group'])
            fund = ""
            if "fund_group" in config and len(datetime_match.groups()) >= config['fund_group']:
                fund = datetime_match.group(config['fund_group'])
            fund_replace = f"-{fund}" if fund else ""
            datetime_fund_pattern = datetime_pattern.replace("$FUND", fund_replace)
            date_full = datetime.datetime.strptime(datetime_str, datetime_fund_pattern)
            timestamp = date_full.strftime("%Y%m%d")
            sheet_info = {
                'timestamp': timestamp,
                'sheet_title': sheet_title,
                'date_full': date_full,
                'fund': fund
            }
            if 'include_data' in config and config['include_data']:
                sheet_info['data'] = pd.DataFrame(self.get_sheet_data(sheet_info['sheet_title']))
            sheet_infos.append(sheet_info)
            sheets_retrieved += 1
            if sheets_retrieved >= sheets_to_retrieve:
                break
        return sheet_infos


def main():
    test_sheet_id = "YOUR SHEET ID HERE"
    sheets = GoogleSheetsImporter(test_sheet_id)
    resp = sheets.get_sheet_id("20210818-094234")
    print(resp)


if __name__ == "__main__":
    main()

